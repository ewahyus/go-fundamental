package main

import (
	"fmt"
)

func main() {
	res := divisibleBy(10, 0)
	fmt.Printf("%v\n", res)
}

func divisibleBy(n, divisor int) bool {
	if divisor == 0 {
		return false
	}

	return (n%divisor == 0)
}
