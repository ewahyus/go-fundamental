package main

import "testing"

func testCalculate(t *testing.T) {
	if Calculate(2) != 4 {
		t.Error("Expected 2 + 2 = 4")
	}
}
