package main

import (
	"fmt"
)

type Player struct {
	ID   int
	Name string
}

func main() {
	player := Player{
		ID:   1,
		Name: "Eko Wahyu S",
	}

	fmt.Println(player)
	fmt.Println(player.ID)
	fmt.Println(player.Name)
}
