package main

import (
	"fmt"
)

func main() {
	// 1
	for i := 0; i <= 10; i++ {
		fmt.Printf("perulangan ke %d\n", i)
	}

	fmt.Println("\n--------------------\n")
	// 2
	x := 10
	var y int

	for y < x {
		y++
		fmt.Println(y)
	}

	fmt.Println("\n--------------------\n")

	// 3 infinity loop
	z := 1
	for {
		z++

		if z == 5 {
			continue
		}

		fmt.Printf("Hello go %d\n", z)

		if z == 10 {
			break
		}
	}
}
