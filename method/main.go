package main

import (
	"fmt"
)

type Person struct {
	ID      int
	Name    string
	Address string
}

// receiver
func (p *Person) GetID() int {
	return p.ID
}

func (p *Person) GetName() string {
	return p.Name
}

func (p *Person) GetAddress() string {
	return p.Address
}

func (p *Person) changeName(newName string) {
	p.Name = newName
}

func main() {
	p := &Person{
		ID:      1,
		Name:    "Eko Wahyu S",
		Address: "Jakarta",
	}

	fmt.Println(p.GetID())
	fmt.Println(p.GetName())
	fmt.Println(p.GetAddress())
	p.changeName("Eko wahyu saputro")
	// after changeName
	fmt.Println(p.GetName())
}
