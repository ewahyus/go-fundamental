package main

import (
	"fmt"
)

// constant adalah sebuah variable yang mempunyai nilai tetap
//global constant
const A string = "ini adalah sebuah constant bertipe data string"

func main() {
	fmt.Println(A)

	const X int = 30
	fmt.Println(X)
}
