package main

import (
	"fmt"
)

func main() {
	//go mempunyai 2 tipe variable : static type dan dinamic type
	//static type
	var x int
	x = 10
	var y float64
	y = 5.5
	fmt.Println(y)
	fmt.Println(x)

	//untuk mengetahui type data dari suatu variable
	fmt.Printf("x memiliki type data %T\n", x)
	fmt.Printf("y memiliki type data %T\n", y)

	//dinamic type declaration
	z := "ini sebuah string"
	fmt.Println(z)
}
