package main

import (
	"fmt"
)

type Person struct {
	ID      int
	Name    string
	Address string
}

func printPerson(p Person) {
	fmt.Println("ID = ", p.ID)
	fmt.Println("Name = ", p.Name)
	fmt.Println("Address = ", p.Address)
}

func main() {
	p := Person{
		ID:      1,
		Name:    "Eko Wahyu S",
		Address: "Jakarta",
	}

	printPerson(p)
}
