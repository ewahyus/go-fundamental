package main

import (
	"fmt"
)

func main() {
	var hello string = "hello"
	var strPtr *string
	strPtr = &hello

	// check memory address
	fmt.Println(&hello)
	fmt.Println(strPtr)
	change(hello)
	fmt.Println(hello)
	changeWithPointer(&hello)
	fmt.Println(hello)
}

func change(v string) {
	v = v + " Golang"
}

func changeWithPointer(v *string) {
	*v = *v + " Golang"
}
