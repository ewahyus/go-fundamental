package main

import (
	"fmt"
)

func main() {
	//jika kondisi true
	if true {
		fmt.Println("ini akan dieksekusi oleh program")
	}

	x := 5
	y := 3
	// kondisi menggunakan aritmatik sebagai perbandingan
	if x > y {
		fmt.Println("ini akan dieksekusi oleh program")
	}

	r := true
	z := false

	if r && z {
		fmt.Println("ini tidak akan dieksekusi oleh program karena bernilai false")
	}

	if r || z {
		fmt.Println("ini akan dieksekusi oleh program")
	}

}
