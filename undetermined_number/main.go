package main

import (
	"fmt"
)

func sum(args ...int) (result int) {
	for _, v := range args {
		result += v
	}
	return
}

func main() {
	fmt.Printf("%d\n", sum(1, 2, 3, 4))
	fmt.Printf("%d\n", sum(1, 2, 3, 4, 5, 6, 7, 8))
}
