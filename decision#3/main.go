package main

import (
	"fmt"
)

func main() {
	x := 100

	// x langsung kita substitusi kedalam keyword switch (expression switch)
	switch x {
	case 60:
		fmt.Println("Nilai = C")
	case 80:
		fmt.Println("Nilai = B")
	case 100:
		fmt.Println("Nilai = A")
	default:
		fmt.Println("Nilai tidak diketahui")
	}

	switch {
	case x == 60:
		fmt.Println("Nilai = C")
	case x == 80:
		fmt.Println("Nilai = B")
	case x == 100:
		fmt.Println("Nilai = A")
	default:
		fmt.Println("Nilai tidak diketahui")
	}

	// type switch
	var y interface{}
	y = 5

	switch y.(type) {
	case int:
		fmt.Println("y bertipe data int")
	case string:
		fmt.Println("y bertipe data string")
	case float64:
		fmt.Println("y bertipe data float64")
	default:
		fmt.Println("y tidak mempunyai tipe data")
	}
}
